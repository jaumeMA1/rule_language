const ts = require('./rule_type_system.js');
const tf = require('./rule_type_factory.js');
const rss = require('./rule_scoped_symbols.js');

class syntax_optimizer
{
	constructor()
	{
		this.scopedSymbols = new rss.scoped_symbols();
		this.typeFactory = new tf.type_factory();
	}
	optimize_unary_expr(expr)
	{
		const predicate = expr.set_expr(this.optimize_expr(expr.expr()));
		
		if(predicate.solvable())
		{
			return this.typeFactory.create_solvable_expression(expr.evaluate(predicate));
		}
		else
		{
			return expr;
		}
	}
	optimize_binary_expr(expr)
	{
		const lhs = expr.set_lhs(this.optimize_expr(expr.lhs_expr()));
		const rhs = expr.set_rhs(this.optimize_expr(expr.rhs_expr()));
		
		if(lhs.solvable() && rhs.solvable())
		{
			return this.typeFactory.create_solvable_expression(expr.evaluate(lhs,rhs));
		}
		else
		{
			return expr;
		}
	}
	optimize_expr(expr)
	{
		switch(expr.type().value())
		{
			case 'unary':
			{
				return this.optimize_unary_expr(expr);
			}
			case 'binary':
			{
				return this.optimize_binary_expr(expr);
			}
			case 'variable':
			{
				const variableDecl = this.scopedSymbols.find(expr.label());
				
				if(variableDecl != undefined)
				{
					return variableDecl.expression();
				}
				else
				{
					return expr;
				}
			}
			default:
			{	
				return expr;
			}
		}
	}
	optimize_stmnt_expr(stmnt)
	{
		for(let [index,expr] of stmnt.expressions())
		{
			stmnt.set_expr(index,this.optimize_expr(expr));
		}
	}
	optimize_assignment_stmnt(stmnt)
	{
		this.scopedSymbols.add(stmnt.label(),stmnt);
		
		this.optimize_stmnt_expr(stmnt);
	}
	optimize_scope_stmnt(stmnt)
	{
		this.scopedSymbols.push([]);
		
		for(const [index,_stmnt] of stmnt)
		{
			this.optimize_stmnt(stmnt,index,_stmnt);
		}

		this.scopedSymbols.pop();
	}
	optimize_case_statement(stmnt)
	{
		this.optimize_stmnt_expr(stmnt);
		this.optimize_scope_stmnt(stmnt);
	}
	optimize_selective_stmnt(stmnt)
	{
		let soFarResolvable = true;
		
		for(const [indexCase,currCase] of stmnt.cases())
		{
			this.optimize_case_statement(currCase);

			if(soFarResolvable && currCase.solvable())
			{
				if(currCase.test({}))
				{
					return currCase;
				}
				else
				{
					stmnt.clear_case(indexCase);
				}
			}
			else
			{
				soFarResolvable = false;

				this.optimize_case_statement(currCase);
			}
		}
		
		return stmnt;
	}
	optimize_foreach_stmnt(stmnt)
	{
		this.optimize_stmnt_expr(stmnt);
		this.optimize_scope_stmnt(stmnt);
		
		const iterableExpr = stmnt.expression();
		
		if(iterableExpr instanceof ts.iterable_expression)
		{
			const newStmnt = new ts.block_statement([]);
			
			for(const arg of stmnt.args())
			{
				const newBlock = new ts.block_statement([]);

				//add unroll args
				stmnt.labels((label,labelIndex) =>
				{
					newBlock.add_stmnt(new ts.assignment_statement([label],arg[labelIndex],false));
				});

				//add statements
				for(const [stmntIndex,_stmnt] of stmnt)
				{
					newBlock.add_stmnt(_stmnt);
				}

				newStmnt.add_stmnt(newBlock);
			}

			return newStmnt;
		}
		else
		{
			return stmnt;
		}
	}
	optimize_stmnt(parentStmnt,index,stmnt)
	{
		if(stmnt instanceof ts.selective_statement)
		{
			parentStmnt.set_stmnt(index,this.optimize_selective_stmnt(stmnt));
		}
		else if(stmnt instanceof ts.foreach_statement)
		{
			parentStmnt.set_stmnt(index,this.optimize_foreach_stmnt(stmnt));
		}
		else if(stmnt instanceof ts.scope_statement)
		{
			this.optimize_scope_stmnt(stmnt);
		}
		else if(stmnt instanceof ts.assignment_statement)
		{
			this.optimize_assignment_stmnt(stmnt);
		}
		else
		{
			this.optimize_stmnt_expr(stmnt);
		}
	}
	optimize(program)
	{
		for(const [ruleId,rule] of program)
		{
			this.optimize_scope_stmnt(rule);
		}

		return program;
	}
};

module.exports.syntax_optimizer = syntax_optimizer;