const ts = require('./rule_type_system.js');
const ss = require('./rule_scoped_symbols.js');

class type_factory
{
	constructor()
	{
		this.scopedSymbols = new ss.scoped_symbols();		
	}
	create_boolean_expression(...args)
	{
		return new ts.boolean_expression(...args);
	}
	create_numeric_expression(...args)
	{
		return new ts.numeric_expression(...args);
	}
	create_literal_expression(...args)
	{
		return new ts.literal_expression(...args);
	}
	create_variable_expression(label)
	{
		if(this.scopedSymbols.has(label))
		{
			return new ts.variable_expression(label);
		}
		else
		{
			throw "Variable " + label + " not declared in current scope";
		}
	}
	create_global_variable_expression(...args)
	{
		return new ts.global_variable_expression(...args);
	}
	create_structured_data_expression(...args)
	{
		return new ts.structured_data_expression(...args);
	}
	create_member_access_expression(variable,member)
	{
		this.scopedSymbols.expand(variable.label());

		if(this.scopedSymbols.has(member))
		{
			this.scopedSymbols.pop();

			return new ts.member_access_expression(variable,new ts.variable_expression(member));
		}
		else
		{
			throw "Member " + member + " not declared in " + variable.label();
		}
	}
	create_member_function_access_expression(...args)
	{
		return new ts.member_function_access_expression(...args);
	}
	create_type_function_access_expression(...args)
	{
		return new ts.type_function_access_expression(...args);
	}
	create_array_access_expression(...args)
	{
		return new ts.array_access_expression(...args);
	}
	create_indexed_access_expression(...args)
	{
		return new ts.indexed_access_expression	(...args);
	}
	create_function_call_expression(program,label,args)
	{
		if(this.scopedSymbols.has(label))
		{
			return new ts.function_call_expression(program,label,args);
		}
		else
		{
			throw "Trying to call function not found: " + label;
		}
	}
	create_array_expression(...args)
	{
		return new ts.array_expression(...args);
	}
	create_unary_expression(...args)
	{
		return new ts.unary_expression(...args);
	}
	create_binary_expression(...args)
	{
		return new ts.binary_expression(...args);
	}
	create_solvable_expression(runtime_object)
	{
		const typeStr = runtime_object.inspect('type').value();
		
		switch(typeStr)
		{
			case 'boolean':
			{
				return this.create_boolean_expression(runtime_object.value());
			}
			case 'numeric':
			{
				return this.create_numeric_expression(runtime_object.value());
			}
			case 'text':
			{
				return this.create_literal_expression(runtime_object.value());
			}
			default:
			{
				throw 'Trying to construct expression from not solvable runtime: ' + typeStr;
			}
		}
	}
	create_expression_statement(...args)
	{
		return new ts.expression_statement(...args);
	}
	create_return_statement(...args)
	{
		return new ts.return_statement(...args);
	}
	create_assignment_statement(labels,stmnt)
	{
		for(const label of labels)
		{
			this.scopedSymbols.add(label,new ts.assignment_statement([label],stmnt));
		}
		
		return new ts.assignment_statement(labels,stmnt);
	}
	create_print_statement(...args)
	{
		return new ts.print_statement(...args);
	}
	create_runtime_error_statement(...args)
	{
		return new ts.runtime_error_statement(...args);
	}
	create_block_statement(...args)
	{
		return new ts.block_statement(...args);
	}
	create_selective_statement(...args)
	{
		return new ts.selective_statement(...args);
	}
	create_while_statement(...args)
	{
		return new ts.while_statement(...args);
	}
	create_foreach_statement(labels,iterableExpr,stmnts)
	{
		let solved = false;

		while(!solved)
		{
			if(iterableExpr.solvable())
			{
				if(iterableExpr instanceof ts.iterable_expression)
				{
					return new ts.foreach_statement(labels,iterableExpr,stmnts);
				}
				else
				{
					throw "Trying to iterate through not iterable expression 1";
				}
			}
			else if(iterableExpr instanceof ts.variable_expression)
			{
				const foundSymbol = this.scopedSymbols.find(iterableExpr.label());
				
				iterableExpr = (foundSymbol != undefined) ? foundSymbol.expression() : iterableExpr;
			}
			else
			{
				solved = true;
			}
		}

		return new ts.foreach_statement(labels,iterableExpr,stmnts);
	}
	create_rule_statement(label,args,stmnts)
	{
		this.scopedSymbols.add(label,args,0);

		return new ts.rule_statement(args,stmnts);
	}
	open_scope(stmnts = [])
	{
		this.scopedSymbols.push(stmnts);
	}
	close_scope()
	{
		this.scopedSymbols.pop();
	}
};

module.exports.type_factory = type_factory;