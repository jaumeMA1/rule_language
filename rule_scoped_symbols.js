const ts = require("./rule_type_system.js");

class arg_symbol
{
	constructor(label)
	{
		this.label = label;
	}
	get(label)
	{
		return this;
	}
	expression()
	{
		return this;
	}
};

class scoped_symbols
{
	constructor()
	{
		this.symbols = [ new Map() ];
	}
	push(args)
	{
		const symbolsLength = this.symbols.push(new Map());

		const currScope = this.symbols[symbolsLength - 1];
		for(const arg of args)
		{
			currScope.set(arg,new arg_symbol(arg));
		}
	}
	pop()
	{
		return this.symbols.pop();
	}
	add(label,statement,index = this.symbols.length - 1)
	{
		if(index <= this.symbols.length)
		{
			const currScope = this.symbols[index];
			
			currScope.set(label,statement);
			
			return statement;
		}
		else
		{
			throw 'Trying to add symbol in empty stack: ' + label;
		}
	}
	find(label)
	{
		for (let scopeIndex=this.symbols.length;scopeIndex>0;scopeIndex--)
		{
			const currScope = this.symbols[scopeIndex - 1];
				
			const foundSymbol = currScope.get(label);

			if(foundSymbol != undefined)
			{
				return foundSymbol;
			}
		}
		
		return undefined;
	}
	has(label)
	{
		return this.find(label) != undefined;
	}
	expand(label)
	{
		for (let scopeIndex=this.symbols.length;scopeIndex>0;scopeIndex--)
		{
			const currScope = this.symbols[scopeIndex - 1];
				
			const foundSymbol = currScope.get(label);

			if(foundSymbol != undefined)
			{
				const foundExpression = foundSymbol.expression();
							
				if(foundExpression instanceof ts.structured_data_expression)
				{
					const structMembers = foundExpression.members();

					this.symbols.push(new Map(structMembers.map((member) => { return [member.label(), member]; })));

					const currScope = this.symbols[this.symbols.length - 1];
				}
				else
				{
					this.symbols.push(new arg_symbol(label));
				}

				break;
			}
		}
	}
};

module.exports.scoped_symbols = scoped_symbols;