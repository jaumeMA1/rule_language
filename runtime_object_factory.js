
const rt = require("./rule_runtime.js");

function create_number_object(number)
{
	return new rt.number_object(number);
}
function create_boolean_object(cond)
{
	return new rt.boolean_object(cond);
}
function create_literal_object(literal)
{
	return new rt.literal_object(literal);
}
function create_array_object(items)
{
	let arrayData = new rt.array_object(items);

	create_visitor(items,(index,value) => { arrayData.add_item(value); })

	return arrayData;
}
function create_structured_object(members)
{
	let structuredData = new rt.structured_object();
	
	create_visitor(members,(key,value) => { structuredData.add_member(key,value); })
	
	return structuredData;
}
function create_indexed_object(expr,index)
{
	const newObj = create_runtime_object(expr);
	
	newObj.add_method('index',() =>
	{
		return create_number_object(index);
	});
	
	return newObj;
}
function create_expression_runtime_object(expr)
{
	if(expr.is_type("text"))
	{
		return create_literal_object(expr.value());
	}
	else if(expr.is_type("numeric"))
	{
		return create_number_object(expr.value());
	}
	else if(expr.is_type("boolean"))
	{
		return create_boolean_object(expr.value());
	}
	else if(expr.is_type("array"))
	{
		return create_array_object(expr.value());
	}
	else if(expr.is_type("struct"))
	{
		return create_structured_object(expr.value());
	}
	else
	{
		throw "Unsupported expression";
	}
}
function create_runtime_object(expr)
{
	if(typeof(expr) == "string")
	{
		return create_literal_object(expr);
	}
	else if(typeof(expr) == "number")
	{
		return create_number_object(expr);
	}
	else if(typeof(expr) == "boolean")
	{
		return create_boolean_object(expr);
	}
	else if(expr instanceof Array)
	{
		return create_array_object(expr);
	}
	else if(expr instanceof rt.runtime_object)
	{
		return expr;
	}
	else
	{
		return create_structured_object(expr);
	}
}
function create_visitor(input,sink)
{
	for (const key in input)
	{
		const value = input[key];

		sink(key,create_runtime_object(value));
	}
}
function create_rule(data)
{
	let newRule = new rt.rule_runtime();

	create_visitor(data,(key,value) => { newRule.add_variable(key,value)});

	return newRule;
}
function create_runtime(data)
{
	let newRuntime = new rt.global_runtime((input,sink) => { create_visitor(input,sink); });

	create_visitor(data,(key,value) => { newRuntime.add_variable(key,value)});

	return newRuntime;
}

module.exports.create_number_object = create_number_object;
module.exports.create_literal_object = create_literal_object;
module.exports.create_boolean_object = create_boolean_object;
module.exports.create_array_object = create_array_object;
module.exports.create_structured_object = create_structured_object;
module.exports.create_rule = create_rule;
module.exports.create_runtime = create_runtime;
module.exports.create_runtime_object = create_runtime_object;
module.exports.create_indexed_object = create_indexed_object;
module.exports.create_expression_runtime_object = create_expression_runtime_object;