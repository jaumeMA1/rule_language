const fs = require("fs");
const jn = require("jison");
const rs = require('./rule_syntax.js');
const rt = require('./runtime_object_factory.js');

var parser = new jn.Parser(fs.readFileSync("rule_system.jison", "utf8"));

const ruleSyntax = new rs.syntax(parser);

const globalRuntime = rt.create_runtime({invalidServiceIndex: -1, sfu_services: {type: 'sfu', maxNumPlayersPerService: 2, entries: []}, mcu_services: {type: 'mcu', maxNumPlayersPerService: 5, entries: []}});

const sfuServices = globalRuntime.find_variable('sfu_services');

if(sfuServices != undefined)
{
	const entries = sfuServices.find_member('entries');
	
	if(entries != undefined)
	{
		entries.add_item(rt.create_runtime_object({players: ['jaume'], url: 'https://127.0.0.1:4949/sfu1'}));
		entries.add_item(rt.create_runtime_object({players: ['joan'], url: 'https://127.0.0.1:4949/sfu2'}));
		entries.add_item(rt.create_runtime_object({players: ['anna','pep'], url: 'https://127.0.0.1:4949/sfu3'}));
	}
}

const mcuServices = globalRuntime.find_variable('mcu_services');

if(mcuServices != undefined)
{
	const entries = mcuServices.find_member('entries');
	
	if(entries != undefined)
	{
		entries.add_item(rt.create_runtime_object({players: ['eduard'], url: 'https://127.0.0.1:4949/mcu1'}));
	}
}

const program = ruleSyntax.compile("./test.rule");

if(program != undefined)
{
	try
	{
		const result = program.execute("on_new_player",globalRuntime.runtime({adeu: 100, playerData: { player_props: { has_gpu: false } } }));

		result.for_each((item) =>
		{
			console.log(item);
		});
	}
	catch(error)
	{
		console.error("Runtime error: " + error);
	}
}

// function(eventId,eventData)
// {
// 	const result = this_program.execute(eventId,globalRuntime.rt(eventData));

// 	result.for_each((item) =>
// 	{
// 		console.log(item);
// 	});
// }