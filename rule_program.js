const rt = require("./runtime_object_factory.js");

class program
{
	constructor()
	{
		this.rules = new Map();
	}
	add_rule(label,rule)
	{
		this.rules.set(label,rule);
	}
	find_rule(label)
	{
		return this.rules.get(label);
	}
	import_program(other)
	{
		other.rules.forEach((value,key) =>
		{
			this.rules.set(key,value);
		});
	}
	instantiate(label,args,runtime)
	{
		const rule = this.rules.get(label);

		if(rule != undefined)
		{
			if(args.length == rule.num_args())
			{
				let nestedRuntime = rt.create_rule();

				//fill nested runtime with input args
				for (let varIndex=0;varIndex<args.length;varIndex++)
				{
					nestedRuntime.add_variable(rule.arg(varIndex),args[varIndex].instantiate(runtime));
				}

				return rule.instantiate(nestedRuntime);
			}

			throw "Mismatch between rule definition of " + label + " and rule call";
		}

		throw "Rule not found: " + label;
	}
	[Symbol.iterator]()
	{
		return this.rules.entries()[Symbol.iterator]();
	}
};

class program_interface
{
	constructor(program_impl)
	{
		this.program_impl = program_impl;
	}
	execute(ruleEvent,runtime)
	{
		const eventRule = this.program_impl.find_rule(ruleEvent);
		
		if(eventRule != undefined)
		{
			return eventRule.instantiate(runtime);
		}

		throw "Rule not found: " + ruleEvent;
	}
};

module.exports.program = program;
module.exports.program_interface = program_interface;
