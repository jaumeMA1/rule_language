
const ts = require("./rule_type_system.js");

class runtime_object
{
	constructor(typeMethods)
	{
		this.typeMethods = typeMethods;
		this.runtimeMethods = new Map();

		this.typeMethods.set('is_type', (args) =>
		{
			let cond = false;
			const method = this.typeMethods.get('type');
			
			if(method != undefined)
			{
				cond = (method(args).value() == args[1].value());
			}

			return new boolean_object(cond);
		});
		this.typeMethods.set('is_iterable', (args) =>
		{
			return new boolean_object(false);
		});
	}
	add_method(label,method)
	{
		this.runtimeMethods.set(label,method);
	}
	inspect(label,args)
	{
		const method = this.typeMethods.get(label);
		
		if(method != undefined)
		{
			return method([this].concat(args));
		}
		
		throw "Type method not found: " + label;
	}
	has(label)
	{
		return this.runtimeMethods.get(label) != undefined;
	}
	evaluate(label,input)
	{
		const method = this.runtimeMethods.get(label);
		
		if(method != undefined)
		{
			return method(input);
		}
		
		throw "Runtime method not found: " + label;
	}
};

class boolean_object extends runtime_object
{
	constructor(cond,typeMethods = ts.boolean_expression.type_methods)
	{
		super(typeMethods);

		this.add_method('not',() =>
		{
			return new boolean_object(!this.cond);
		});
		this.add_method('and',(input) =>
		{
			return new boolean_object(this.cond && input.other.cond);
		});
		this.add_method('or',(input) =>
		{
			return new boolean_object(this.cond || input.other.cond);
		});
		this.add_method('equal',(input) =>
		{
			return new boolean_object(this.cond == input.other.cond);
		});

		this.cond = Boolean(cond);
	}
	value()
	{
		return this.cond;
	}
	bool()
	{
		return this.cond;
	}
	as_bool()
	{
		return this;
	}
}

class number_object extends boolean_object
{
	constructor(number)
	{
		super(number,ts.numeric_expression.type_methods);
	
		this.number = number;

		this.add_method('equal',(input) =>
		{
			return new boolean_object(this.number == input.other.as_number().value());
		});
		this.add_method('not_equal',(input) =>
		{
			return new boolean_object(this.number != input.other.as_number().value());
		});
		this.add_method('less',(input) =>
		{
			return new boolean_object(this.number < input.other.as_number().value());
		});
		this.add_method('greater',(input) =>
		{
			return new boolean_object(this.number > input.other.as_number().value());
		});
		this.add_method('less_or_equal',(input) =>
		{
			return new boolean_object(this.number <= input.other.as_number().value());
		});
		this.add_method('greater_or_equal',(input) =>
		{
			return new boolean_object(this.number >= input.other.as_number().value());
		});
		this.add_method('inv',() =>
		{
			return new number_object(-this.number);
		});
		this.add_method('sum',(input) =>
		{
			if (input.other instanceof literal_object)
			{
				return new literal_object(this.as_str().value() + input.other.value());
			}
			else
			{
				return new number_object(this.number + input.other.as_number().value());
			}
		});
		this.add_method('subs',(input) =>
		{
			return new number_object(this.number - input.other.as_number().value());
		});
		this.add_method('prod',(input) =>
		{
			return new number_object(this.number * input.other.as_number().value());
		});
		this.add_method('div',(input) =>
		{
			return new number_object(this.number / input.other.as_number().value());
		});
		this.add_method('pow',(input) =>
		{
			return new number_object(this.number ^ input.other.as_number().value());
		});
		this.add_method('as_bool',() =>
		{
			return this.as_bool();
		});
		this.add_method('as_number',() =>
		{
			return this.as_number();
		});
		this.add_method('as_str',() =>
		{
			return this.as_str();
		});
	}
	value()
	{
		return this.number;
	}
	as_number()
	{
		return this;
	}
	as_str()
	{
		return new literal_object(String(this.number));
	}
};

class literal_object extends boolean_object
{
	constructor(literal)
	{
		super(literal,ts.literal_expression.type_methods);

		this.literal = literal;

		this.add_method('equal',(input) =>
		{
			return new boolean_object(this.literal == input.other.as_str().value());
		});
		this.add_method('not_equal',(input) =>
		{
			return new boolean_object(this.literal != input.other.as_str().value());
		});
		this.add_method('less',(input) =>
		{
			return new boolean_object(this.literal < input.other.as_str().value());
		});
		this.add_method('greater',(input) =>
		{
			return new boolean_object(this.literal > input.other.as_str().value());
		});
		this.add_method('less_or_equal',(input) =>
		{
			return new boolean_object(this.literal <= input.other.as_str().value());
		});
		this.add_method('greater_or_equal',(input) =>
		{
			return new boolean_object(this.literal >= input.other.as_str().value());
		});
		this.add_method('sum',(input) =>
		{
			return new literal_object(this.literal + input.other.as_str().value());
		});
		this.add_method('as_bool',() =>
		{
			return this.as_bool();
		});
		this.add_method('as_str',() =>
		{
			return this.as_str();
		});
		this.add_method('as_number',() =>
		{
			return this.as_number();
		});
	}
	value()
	{
		return this.literal;
	}
	as_number()
	{
		return new number_object(Number(this.literal));
	}
	as_str()
	{
		return this;
	}
};

class iterable_object extends runtime_object
{
	constructor(typeMethods,entries)
	{
		super(typeMethods);

		this.items = entries;

		this.add_method('iterate',(callable) =>
		{
			this.for_each(callable);
		});
		this.add_method('length',() =>
		{
			return new number_object(this.length());
		});
		this.add_method('empty',() =>
		{
			return new boolean_object(this.length() == 0);
		});
	}
	empty()
	{
		return this.length() == 0;
	}
	[Symbol.iterator]()
	{
		return this.items.entries()[Symbol.iterator]();
	}
	for_each(callable)
	{
		this.items.forEach(callable)
	}
};

class structured_object extends iterable_object
{
	constructor()
	{
		super(ts.structured_data_expression.type_methods,new Map());

		this.typeMethods.set('is_iterable', (args) =>
		{
			return new boolean_object(true);
		});
		this.add_method('get',(label) =>
		{
			return this.find_member(label);
		});
	}
	add_member(label,obj)
	{
		this.items.set(label,obj);
	}
	find_member(label)
	{
		return this.items.get(label);
	}
	find_variable(label)
	{
		return this.find_member(label);
	}
	length()
	{
		return this.items.size;
	}
	print()
	{
		console.log([...this.items.entries()]);
	}
}

class array_object extends iterable_object
{
	constructor()
	{
		super(ts.array_expression.type_methods,[]);

		this.typeMethods.set('is_iterable', (args) =>
		{
			return new boolean_object(true);
		});
		this.add_method('begin',() =>
		{
			return new number_object(this.items.length);
		});
		this.add_method('last',() =>
		{
			return new number_object(this.items.length);
		});
		this.add_method('get',(index) =>
		{
			return this.item(index);
		});
	}
	add_item(item)
	{
		this.items.push(item);
	}
	remove_item(index)
	{
		if(index < this.items.length)
		{
			this.items.splice(index,1);
		}
		else
		{
			throw "Index out of bounds: " + index;
		}
	};
	item(index)
	{
		if(index >= 0 && index < this.items.length)
		{
			return this.items[index];
		}
		
		throw "Index out of bounds: " + index;
	}
	find_item(label)
	{
		return this.items.find((value) => { return value == label; });
	}
	length()
	{
		return this.items.length;
	}
};

class runtime_scope
{
	constructor()
	{
		this.variables = new Map();
	}
	find_variable(label)
	{
		return this.variables.get(label);
	}
	add_variable(label,obj)
	{
		this.variables.set(label,obj);
	}
	print()
	{
		console.log([...this.variables.entries()]);
	}
} global_scope = new runtime_scope();

class rule_runtime
{
	constructor()
	{
		this.scope = [ new runtime_scope() ];
	}
	push_scope()
	{
		this.scope.push(new runtime_scope());
	}
	pop_scope()
	{
		this.scope.pop();
	}
	find_variable(label)
	{
		for (let index = this.scope.length; index > 0; index--)
		{
			const variable = this.scope[index-1].find_variable(label);
			
			if(variable != undefined)
			{
				return variable;
			}
		}

		throw "Variable not found: " + label;
	}
	add_variable(label,obj)
	{
		if(obj != undefined)
		{
			let scopeIndex = this.scope.length - 1;

			for (let index = this.scope.length; index > 0; index--)
			{
				const variable = this.scope[index-1].find_variable(label);
				
				if(variable != undefined)
				{
					scopeIndex = index - 1;

					break;
				}
			}

			this.scope[scopeIndex].add_variable(label,obj);
		}
		else
		{
			throw "Trying to add undefined variable " + label;
		}
	}
};

class global_runtime
{
	constructor(objFactory)
	{
		this.objFactory = objFactory;
	}
	runtime(input)
	{
		let newRuntime = new rule_runtime();
		
		this.objFactory(input,(key,value) => { newRuntime.add_variable(key,value); } );

		return newRuntime;
	}
	find_variable(label)
	{
		const variable = global_scope.find_variable(label);
		
		if(variable != undefined)
		{
			return variable;
		}

		throw "Global variable not found: " + label;
	}
	add_variable(label,obj)
	{
		global_scope.add_variable(label,obj);
	}
};

module.exports.runtime_object = runtime_object;
module.exports.global_runtime = global_runtime;
module.exports.rule_runtime = rule_runtime;
module.exports.array_object = array_object;
module.exports.structured_object = structured_object;
module.exports.literal_object = literal_object;
module.exports.number_object = number_object;
module.exports.boolean_object = boolean_object;