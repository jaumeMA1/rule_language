
const rt = require("./runtime_object_factory.js");

class expression
{
	constructor(instantiator,type_methods)
	{
		this.instantiator = instantiator;
		this.type_methods = type_methods;
	}
	instantiate(runtime)
	{
		return this.instantiator(runtime);
	}
	type()
	{
		const typeMethod = this.type_methods('type');

		if(typeMethod != undefined)
		{
			return typeMethod();
		}
		else
		{
			throw 'Type method not defined';
		}
	}
	is_type(label)
	{
		const typeMethod = this.type_methods('type');

		if(typeMethod != undefined)
		{
			return typeMethod().value() == label;
		}
		else
		{
			throw 'Type method not defined';
		}
	}
	solvable()
	{
		return this.type_methods('value') != undefined;
	}
}

class numeric_expression extends expression
{
	static type_methods = new Map([['type',(expr) => { return rt.create_literal_object("numeric"); }],['value',(expr) => { return expr.value(); }]]);

	constructor(number)
	{
		super((runtime) =>
		{
			return rt.create_number_object(number);
		},(label) =>
		{
			return numeric_expression.type_methods.get(label);
		});
		
		this.number = number;
	}
	value()
	{
		return this.number;
	}
};

class boolean_expression extends expression
{
	static type_methods = new Map([['type',(expr) => { return rt.create_literal_object("boolean"); }],['value',(expr) => { return expr.value(); }]]);

	constructor(cond)
	{
		super((runtime) =>
		{
			return rt.create_boolean_object(cond);
		},(label) =>
		{
			return boolean_expression.type_methods.get(label);
		});

		this.cond = cond;
	}
	value()
	{
		return this.cond;
	}
};

class literal_expression extends expression
{
	static type_methods = new Map([['type',(expr) => { return rt.create_literal_object("text"); }],['value',(expr) => { return expr.value(); }]]);

	constructor(str)
	{
		super((runtime) =>
		{
			return rt.create_literal_object(str);
		},(label) =>
		{
			return literal_expression.type_methods.get(label);
		})

		this.str = str;
	}
	value()
	{
		return this.str;
	}
};

class iterable_expression extends expression
{
	constructor(instantiator,typeMethods,items)
	{
		super(instantiator,typeMethods);

		this.items = items;
	}
	length()
	{
		return this.items.length;
	}
	value()
	{
		return this.items;
	}
	[Symbol.iterator]()
	{
		return this.items.entries()[Symbol.iterator]();
	}
};

class array_expression extends iterable_expression
{
	static type_methods = new Map([['type',(expr) => { return rt.create_literal_object("array"); }],['value',(expr) => { return expr.value(); }]]);

	constructor(items)
	{
		super((runtime) =>
		{
			return rt.create_array_object(items.map((item,index) => { return item.instantiate(runtime); }));
		},(label) =>
		{
			return array_expression.type_methods.get(label);
		},items)
	}
};

class structured_data_expression extends iterable_expression
{
	static type_methods = new Map([['type',(args) => { return rt.create_literal_object("struct"); }],['value',(expr) => { return expr.value(); }],
								   ['has_member',(args) => { if(args.length == 2) { return rt.create_boolean_object(args[0].items.get(args[1].value()) != undefined); } else { throw 'Arguments mismatch in has_member call'; } }]]);

	constructor(members)
	{
		super((runtime) =>
		{
			return rt.create_structured_object(Object.fromEntries(this._members.map((memberAssign) => { return [ memberAssign.label(),memberAssign.expression().instantiate(runtime) ]; })));
		},(label) =>
		{
			return structured_data_expression.type_methods.get(label);
		},members);

		this._members = members.map((memberPair) => { return new assignment_statement([ memberPair[0] ],memberPair[1]) });
	}
	members()
	{
		return this._members;
	}
};

class variable_expression extends expression
{
	static type_methods = new Map([['type',(args) => { return rt.create_literal_object("variable"); }]]);

	constructor(label)
	{
		super((runtime) =>
		{
			return runtime.find_variable(label);
		},(label) =>
		{
			return variable_expression.type_methods.get(label);
		});
		
		this._label = label;
	}
	label()
	{
		return this._label;
	}
};

class global_variable_expression extends expression
{
	static type_methods = new Map([['type',(args) => { return rt.create_literal_object("global_variable"); }]]);

	constructor(label)
	{
		super((runtime) =>
		{
			const variable = global_scope.find_variable(label);
			
			if(variable != undefined)
			{
				return variable;
			}
			else
			{
				throw "Global variable " + label + "not found";
			}
		},(label) =>
		{
			return global_variable_expression.type_methods.get(label);
		});

		this._label = label;
	}
	label()
	{
		return this._label;
	}
};

class function_call_expression extends expression
{
	static type_methods = new Map([['type',(args) => { return rt.create_literal_object("function_call"); }]]);

	constructor(scope,label,args)
	{
		super((runtime) =>
		{
			return scope.instantiate(label,args,runtime);
		},(label) =>
		{
			return function_call_expression.type_methods.get(label);
		});
	}
};

class global_function_call_expression extends expression
{
	static type_methods = new Map([['type',(args) => { return rt.create_literal_object("global_function_call"); }]]);

	constructor(scope,label,args)
	{
		super((runtime) =>
		{
			return scope.instantiate(label,args,runtime);
		},(label) =>
		{
			return global_function_call_expression.type_methods.get(label);
		});
	}
};

class member_access_expression extends expression
{
	static type_methods = new Map([['type',(args) => { return rt.create_literal_object("member_access"); }]]);

	constructor(scope,expr)
	{
		super((runtime) =>
		{
			const member = this.expr.instantiate(scope.instantiate(runtime));
			
			if(member != undefined)
			{
				return member;
			}
			else
			{
				throw "Member not found in " + scope.label();
			}
		},(label) =>
		{
			return member_access_expression.type_methods.get(label);
		});

		this.expr = expr;
	}
	label()
	{
		return this.expr.label();
	}
};

class member_function_access_expression extends expression
{
	static type_methods = new Map([['type',(args) => { return rt.create_literal_object("member_function_access"); }]]);

	constructor(type,member,args)
	{
		super((runtime) =>
		{
			return type.instantiate(runtime).evaluate(member,args.map((arg) => { return arg.instantiate(runtime); }));
		},(label) =>
		{
			return member_function_access_expression.type_methods.get(label);
		});
	}
};

class type_function_access_expression extends expression
{
	static type_methods = new Map([['type',(args) => { return rt.create_literal_object("type_function_access"); }]]);

	constructor(type,member,args)
	{
		super((runtime) =>
		{
			return type.instantiate(runtime).inspect(member,args.map((arg) => { return arg.instantiate(runtime); }));
		},(label) =>
		{
			return type_function_access_expression.type_methods.get(label);
		});
	}
};

class array_access_expression extends expression
{
	static type_methods = new Map([['type',(args) => { return rt.create_literal_object("array_access"); }]]);

	constructor(array,indexExpr)
	{
		super((runtime) =>
		{
			return array.instantiate(runtime).item(indexExpr.instantiate(runtime).value());
		},(label) =>
		{
			return array_access_expression.type_methods.get(label);
		});
	}
};

class indexed_access_expression extends expression
{
	static type_methods = new Map([['type',(args) => { return rt.create_literal_object("indexed_access"); }]]);

	constructor(expr)
	{
		super((runtime) =>
		{
			return expr.instantiate(runtime).evaluate('index');
		},(label) =>
		{
			return indexed_access_expression.type_methods.get(label);
		});
	}
};

class unary_expression extends expression
{
	static type_methods = new Map([['type',(args) => { return rt.create_literal_object("unary"); }]]);

	constructor(expr,op)
	{
		super((runtime) =>
		{
			return this.op(expr.instantiate(runtime));
		},(label) =>
		{
			return unary_expression.type_methods.get(label);
		});
		
		this._expr = expr;
		this.op = op;
	}
	set_expr(expr)
	{
		this._expr = expr;

		return this._expr;
	}
	expr()
	{
		return this._expr;
	}
	evaluate(expr)
	{
		return this.op(expr.instantiate({}));
	}
};

class binary_expression extends expression
{
	static type_methods = new Map([['type',(args) => { return rt.create_literal_object("binary"); }]]);

	constructor(lhsExpr,rhsExpr,op)
	{
		super((runtime) =>
		{
			return op(lhsExpr.instantiate(runtime),() => { return rhsExpr.instantiate(runtime); });
		},(label) =>
		{
			return binary_expression.type_methods.get(label);
		});
	
		this.lhs = lhsExpr;
		this.rhs = rhsExpr;
		this.op = op;
	}
	set_lhs(expr)
	{
		this.lhs = expr;
	
		return this.lhs;
	}
	set_rhs(expr)
	{
		this.rhs = expr;
		
		return this.rhs;
	}
	lhs_expr()
	{
		return this.lhs;
	}
	rhs_expr()
	{
		return this.rhs;
	}
	evaluate(lhs,rhs)
	{
		return this.op(lhs.instantiate({}),() => { return rhs.instantiate({}); });
	}
};

class statement
{
	constructor(instantiator,exprGenerator)
	{
		this.instantiator = instantiator;
		this.exprGenerator = exprGenerator;
	}
	instantiate(runtime)
	{
		return this.instantiator(runtime);
	}
};

class expression_statement extends statement
{
	constructor(expr)
	{
		super((runtime) =>
		{
			this.expr.instantiate(runtime);
			
			return undefined;
		});
	
		this.expr = expr;
	}
	set_expr(index,expr)
	{
		if(index == 0)
		{
			this.expr = expr;
		}
		else
		{
			throw 'Index out of bounds';
		}
	}
	*expressions()
	{
		yield [0,this.expr];
	}
}

class return_statement extends statement
{
	constructor(expr)
	{
		super((runtime) =>
		{
			return this.expr.instantiate(runtime);
		});
		
		this.expr = expr;
	}
	set_expr(index,expr)
	{
		if(index == 0)
		{
			this.expr = expr;
		}
		else
		{
			throw 'Index out of bounds';
		}
	}
	*expressions()
	{
		yield [0,this.expr];
	}
}

class assignment_statement extends statement
{
	constructor(labels,expr)
	{
		super((runtime) =>
		{
			const exprRuntime = this.expr.instantiate(runtime);

			if(this.labels.length == 1)
			{
				runtime.add_variable(this.labels[0],exprRuntime);
			}
			else
			{
				if(exprRuntime.inspect('is_iterable').bool())
				{
					if(this.labels.length == exprRuntime.length())
					{
						let labelIndex = 0;
						for(const [key,value] of exprRuntime)
						{
							runtime.add_variable(this.labels[labelIndex++],value);
						}
					}
					else
					{
						throw "Mismatch in assignment binding";
					}
				}
				else
				{
					throw "Binding variables to non iterable value: " + labels;
				}
			}
			
			return undefined;
		});
		
		this.labels = labels;
		this.expr = expr;
	}
	label()
	{
		return this.labels[0];
	}
	expression()
	{
		return this.expr;
	}
	set_expr(index,expr)
	{
		if(index == 0)
		{
			this.expr = expr;
		}
		else
		{
			throw 'Index out of bounds';
		}
	}
	*expressions()
	{
		yield [0,this.expr];
	}
};

class print_statement extends statement
{
	constructor(expr)
	{
		super((runtime) =>
		{
			console.log(this.expression.instantiate(runtime).value());
		});

		this.expression = expr;
	}
	set_expr(index,expr)
	{
		if(index == 0)
		{
			this.expression = expr;
		}
		else
		{
			throw 'Index out of bounds';
		}
	}
	*expressions()
	{
		return [0,this.expression];
	}
};

class runtime_error_statement extends statement
{
	constructor(expr)
	{
		super((runtime) =>
		{
			throw this.expression.instantiate(runtime).value();
		});
		
		this.expression = expr;
	}
	set_expr(index,expr)
	{
		if(index == 0)
		{
			this.expression = expr;
		}
		else
		{
			throw 'Index out of bounds';
		}
	}
	*expressions()
	{
		return [0,this.expression];
	}
};

class scope_statement extends statement
{
	constructor(instantiator,stmnts)
	{
		super(instantiator);
		
		this.stmnts = stmnts;
	}
	scoped_instantiate(runtime)
	{
		runtime.push_scope();

		for (const stmnt of this.stmnts)
		{
			const retVal = stmnt.instantiate(runtime);
			
			if(retVal != undefined)
			{
				runtime.pop_scope();
				
				return retVal;
			}
		}
		
		runtime.pop_scope();

		return undefined;
	}
	add_stmnt(stmnt)
	{
		this.stmnts.push(stmnt);
	}
	set_stmnt(index,stmnt)
	{
		if(index < this.stmnts.length)
		{
			this.stmnts[index] = stmnt;
		}
		else
		{
			throw 'Index out of bounds';
		}
	}
	get_statement(index)
	{
		if(index < this.stmnts.length)
		{
			return this.stmnts[index];
		}
		else
		{
			throw 'Index out of bounds';
		}
	}
	length()
	{
		return this.stmnts.length;
	}
	empty()
	{
		return this.stmnts.length == 0;
	}
	[Symbol.iterator]()
	{
		return this.stmnts.entries()[Symbol.iterator]();
	}
};

class block_statement extends scope_statement
{
	constructor(stmnts)
	{
		super((runtime) =>
		{
			return this.scoped_instantiate(runtime);
		},stmnts);
	}
};

class case_statement extends block_statement
{
	constructor(condExpr,stmnts)
	{
		super(stmnts);
		
		this.condExpr = condExpr;
	}
	set_condition(expr)
	{
		this.condExpr = expr;
	}
	condition()
	{
		return this.condExpr;
	}
	solvable()
	{
		return this.condExpr.solvable();
	}
	test(runtime)
	{
		return this.condExpr.instantiate(runtime).bool();
	}
	set_expr(index,expr)
	{
		if(index == 0)
		{
			this.condExpr = expr;
		}
		else
		{
			throw 'Index out of bounds';
		}
	}
	*expressions()
	{
		yield [0,this.condExpr];			
	}
};

class selective_statement extends statement
{
	constructor(cases,defaultStmnts)
	{
		super((runtime) =>
		{
			for (let caseIndex=0;caseIndex<this._cases.length;caseIndex++)
			{
				const currCase = this._cases[caseIndex];
				
				if(currCase.test(runtime))
				{
					return currCase.instantiate(runtime);
				}	
			}
			
			return undefined;
		});
		
		this._cases = cases.map((value) => { return new case_statement(value[0],value[1]); });
		this._cases.push(new case_statement(new boolean_expression(true),defaultStmnts));
	}
	clear_case(index)
	{
		if(index < this._cases.length)
		{
			this._cases.splice(index,1);
		}
		else
		{
			throw 'Index out of bounds';
		}
	}
	cases()
	{
		return this._cases.entries();
	}
};

class while_statement extends scope_statement
{
	constructor(condExpr,stmnts)
	{
		super((runtime) =>
		{
			while(this.condExpr.instantiate(runtime).bool())
			{
				const retVal = this.scoped_instantiate(runtime);
				
				if(retVal != undefined)
				{
					return retVal;
				}
			}
			
			return undefined;
		},stmnts);
		
		this.condExpr = condExpr;
	}
	set_expr(index,expr)
	{
		if(index == 0)
		{
			this.condExpr = expr;
		}
		else
		{
			throw 'Index out of bounds';
		}
	}
	solvable()
	{
		return this.condExpr.solvable();
	}
	*expressions()
	{
		yield [0,this.condExpr];
	}
};

class foreach_statement extends scope_statement
{
	constructor(labels,iterableExpr,stmnts)
	{
		super((runtime) =>
		{
			const runtimeObj = this.iterableExpr.instantiate(runtime);
			
			if(runtimeObj.has('iterate'))
			{
				for(const entry of runtimeObj)
				{
					if(this._labels.length == entry.length - 1)
					{
						runtime.push_scope();

						this._labels.forEach((label,labelIndex) =>
						{
							runtime.add_variable(label,rt.create_indexed_object(entry[labelIndex+1],entry[0]));
						});

						const retVal = this.scoped_instantiate(runtime);
						
						runtime.pop_scope();

						if(retVal != undefined)
						{
							return retVal;
						}
					}
					else
					{
						throw "Wrong iterable binding: comparing " + this._labels.length + ' and ' + JSON.stringify(entry);
					}
				}

				return undefined;
			}
			else
			{
				throw "Trying to iterate through not iterable object";
			}
		},stmnts);
		
		this._labels = labels;
		this.iterableExpr = iterableExpr;
	}
	solvable()
	{
		return this.iterableExpr.solvable();
	}
	*args()
	{
		for(const arg of this.iterableExpr)
		{
			arg.splice(0,1);
			
			yield arg;
		}
	}
	set_expr(index,expr)
	{
		if(index == 0)
		{
			this.iterableExpr = expr;
		}
		else
		{
			throw 'Index out of bounds';
		}
	}
	labels(sink)
	{
		this._labels.forEach(sink);
	}
	expression()
	{
		return this.iterableExpr;
	}
	*expressions()
	{
		yield [0,this.iterableExpr];
	}
};

class rule_statement extends scope_statement
{
	constructor(args,stmnts)
	{
		super((runtime) =>
		{
			return this.scoped_instantiate(runtime);
		},stmnts);

		this.args = args;
	}
	get_args()
	{
		return this.args;
	}
	arg(index)
	{
		return this.args[index];
	}
	num_args()
	{
		return this.args.length;
	}
};

module.exports.boolean_expression = boolean_expression;
module.exports.numeric_expression = numeric_expression;
module.exports.literal_expression = literal_expression;
module.exports.variable_expression = variable_expression;
module.exports.global_variable_expression = global_variable_expression;
module.exports.iterable_expression = iterable_expression;
module.exports.array_expression = array_expression;
module.exports.structured_data_expression = structured_data_expression;
module.exports.member_access_expression = member_access_expression;
module.exports.member_function_access_expression = member_function_access_expression;
module.exports.type_function_access_expression = type_function_access_expression;
module.exports.array_access_expression = array_access_expression;
module.exports.indexed_access_expression = indexed_access_expression;
module.exports.function_call_expression = function_call_expression;
module.exports.unary_expression = unary_expression;
module.exports.binary_expression = binary_expression;
module.exports.expression_statement = expression_statement;
module.exports.return_statement = return_statement;
module.exports.assignment_statement = assignment_statement;
module.exports.print_statement = print_statement;
module.exports.runtime_error_statement = runtime_error_statement;
module.exports.scope_statement = scope_statement;
module.exports.block_statement = block_statement;
module.exports.selective_statement = selective_statement;
module.exports.while_statement = while_statement;
module.exports.foreach_statement = foreach_statement;
module.exports.rule_statement = rule_statement;