/* description: Parses end executes mathematical expressions. */

/* lexical grammar */
%lex
%options flex

%{
  yy.keywords = {
	"true": "TRUE",
	"false": "FALSE",
	"if": "IF",
	"else": "ELSE",
	"return": "RETURN",
	"while": "WHILE",
	"import": "IMPORT",
	"which_type": "WHICH_TYPE",
	"for_each": "FOR_EACH",
	"runtime_error": "ERROR",
	"print": "PRINT"
  };
%}

%s literal_definition comment_definition

%%

<comment_definition>[^\n]+ {}
<comment_definition>"\n" { yy.parser.line_number++;	this.popState(); }

<literal_definition>[^\"]+ { return "LITERAL_TEXT"; }
<literal_definition>"\"" { this.popState(); return "LITERAL"; }

[^\S\n]+ { } /* skip whitespace */
\n	{ yy.parser.line_number++; }
\t+ 				  /* skip tabs */
"+" { return "ADDITION"; }
"-" { return "SUBSTRACTION"; }
"*" { return "MULTIPLICATION"; }
"/" { return "DIVISION"; }
"^" { return "POW"; }
"," { return "COMMA"; }
"%"	{ return "PERCENTAGE"; }
"#" { return "SHARP"; }
"["	{ return "OPEN_CLAUDATOR"; }
"]"	{ return "CLOSE_CLAUDATOR"; }
"{" { return "OPEN_CLAUSE"; }
"}" { return "CLOSE_CLAUSE"; }
"<" { return "OPEN_BRACKET"; }
">" { return "CLOSE_BRACKET"; }
"->" { return "RIGHT_ARROW"; }
"!" { return "NOT";}
"<<" { return "LEFT_SHIFT"; }
">>" { return "RIGHT_SHIFT"; }
"." { return "DOT"; }
"(" { return "LEFT_PARENT"; }
")" { return "RIGHT_PARENT"; }
"\"" { this.pushState("literal_definition"); return "LITERAL"; }
"=" { return "ASSIGNMENT"; }
"==" { return "EQUAL";}
"!=" { return "NOT_EQUAL";}
"&&" { return "AND";}
"||" { return "OR";}
"<=" { return "LESS_OR_EQUAL";}
">=" { return "GREATER_OR_EQUAL";}
[a-zA-Z_][a-zA-Z0-9_]* { return (yytext in yy.keywords) ? yy.keywords[yytext] : "IDENT"; }
[0-9]+  { return "NUMBER"; }
":" { return "COLON"; }
";" { return "SEMICOLON"; }
"//" 				{ this.pushState("comment_definition"); }
<<EOF>>               { return 'EOF'; }
.						{ console.log("Unrecognized text: " + yytext); }

/lex

/* operator associations and precedence */

%{
	console.log(__dirname);
%}

%token ADDITION SUBSTRACTION MULTIPLICATION DIVISION POW NOT

%left DOT RIGHT_ARROW
%left ADDITION SUBSTRACTION
%left MULTIPLICATION DIVISION
%left POW
%left NOT
%left PERCENTAGE AND OR EQUAL NOT_EQUAL OPEN_BRACKET CLOSE_BRACKET LESS_OR_EQUAL GREATER_OR_EQUAL
%right ASSIGNMENT COMMA IDENT

%start program

%% /* language grammar */

program: imports rules EOF
{ 
	return $2;
} | rules EOF
{
	return $1;
};

imports: imports import_statement
{
} | import_statement
{
};

import_statement: IMPORT LITERAL LITERAL_TEXT LITERAL SEMICOLON
{
	var fs = require("fs");

	var program = fs.readFileSync($3, "utf8");

	const prevLineNunmber = yy.parser.line_number;
	const prevFileName = yy.parser.file_name;

	yy.parser.line_number = 1;
	yy.parser.file_name = $3;

	$$ = yy.parser.parse(program);

	yy.parser.line_number = prevLineNunmber;
	yy.parser.file_name = prevFileName;
};

rules: rules rule
{
	$$ = $1.concat($2);
} | rule
{
	$$ = $1;
};

rule: rule_header COLON rule_body
{
	$$ = [ yy.program.add_rule($1[0],yy.type_factory.create_rule_statement($1[0],$1[1],$3)) ];

	yy.type_factory.close_scope();
};

rule_header: IDENT
{
	yy.type_factory.open_scope();

	$$ = [$1,[]];
} | IDENT OPEN_BRACKET rule_args CLOSE_BRACKET
{
	yy.type_factory.open_scope($3);

	$$ = [$1,$3];
};

rule_body: OPEN_CLAUSE statements CLOSE_CLAUSE
{
	$$ = $2;
} | OPEN_CLAUSE CLOSE_CLAUSE
{
	$$ = [];
};

rule_args: rule_args IDENT
{
	return $1.concat($2);
} | IDENT
{
	$$ = [ $1 ];
};

statements: statements statement
{
	$$ = $1.concat($2);
} | statement
{
	$$ = [ $1 ];
};

statement: expression_statement SEMICOLON
{
	$$ = $1;
} | selective_statement 
{
	$$ = $1;
} | loop_statement
{
	$$ = $1;
} | return_statement SEMICOLON
{
	$$ = $1;
} | assign_statement SEMICOLON
{
	$$ = $1;
} | print_statement SEMICOLON
{
	$$ = $1;
} | error_statement SEMICOLON
{
	$$ = $1;
};

expression_statement: expression
{
	$$ = yy.type_factory.create_expression_statement($1);
};

assign_statement: IDENT ASSIGNMENT expression
{
	$$ = yy.type_factory.create_assignment_statement([$1],$3);
} | OPEN_CLAUDATOR idents CLOSE_CLAUDATOR ASSIGNMENT expression
{
	$$ = yy.type_factory.create_assignment_statement($2,$5);
};

print_statement: PRINT expression
{
	$$ = yy.type_factory.create_print_statement($2);
};

error_statement: ERROR expression
{
	$$ = yy.type_factory.create_runtime_error_statement($2);
};

selective_statement: selective_header OPEN_CLAUSE statements CLOSE_CLAUSE ELSE OPEN_CLAUSE statements CLOSE_CLAUSE
{
	$$ = yy.type_factory.create_selective_statement([[$1,$3]],$7);

	yy.type_factory.close_scope();
} | selective_header OPEN_CLAUSE statements CLOSE_CLAUSE
{
	$$ = yy.type_factory.create_selective_statement([[$1,$3]],[]);

	yy.type_factory.close_scope();
} | selective_header OPEN_CLAUSE which_type_cases CLOSE_CLAUSE
{
	$$ = yy.type_factory.create_selective_statement($3.map((valuePair) => { return [ yy.type_factory.create_binary_expression($1,valuePair[0],(lhs,rhsFunctor)=>{ const rhsObj = rhsFunctor(); return lhs.inspect('is_type',[rhsObj]); }),valuePair[1]]; }),[]);

	yy.type_factory.close_scope();
};

selective_header: IF LEFT_PARENT expression RIGHT_PARENT
{
	yy.type_factory.open_scope();

	$$ = $3;
} | WHICH_TYPE LEFT_PARENT expression RIGHT_PARENT
{
	yy.type_factory.open_scope();

	$$ = $3;
};

which_type_cases: which_type_cases IDENT COLON OPEN_CLAUSE statements CLOSE_CLAUSE
{
	$$ = $1.concat([ [yy.type_factory.create_literal_expression($2),$5] ]);
} | IDENT COLON OPEN_CLAUSE statements CLOSE_CLAUSE
{
	$$ = [ [yy.type_factory.create_literal_expression($1),$4] ];
};

loop_statement: while_statement
{
	$$ = $1;
} | for_each_statement
{
	$$ = $1;
};

while_statement : while_header OPEN_CLAUSE statements CLOSE_CLAUSE
{
	$$ = yy.type_factory.create_while_statement($1,$3);

	yy.type_factory.close_scope();
};

while_header: WHILE LEFT_PARENT expression RIGHT_PARENT
{
	yy.type_factory.open_scope();

	$$ = $2;
};

for_each_statement: for_each_header OPEN_CLAUSE statements CLOSE_CLAUSE
{
	$$ = yy.type_factory.create_foreach_statement($1[0],$1[1],$3);

	yy.type_factory.close_scope();
};

for_each_header: FOR_EACH LEFT_PARENT IDENT COLON expression RIGHT_PARENT
{
	yy.type_factory.open_scope([$3]);

	$$ = [[$3],$5];

} |  FOR_EACH LEFT_PARENT OPEN_CLAUDATOR idents CLOSE_CLAUDATOR COLON expression RIGHT_PARENT
{
	yy.type_factory.open_scope($4);

	$$ = [$4,$7];
};

expression: symbol_expression
{
	$$ = $1;
} | arithmetic_expression
{
	$$ = $1;
} | boolean_expression
{
	$$ = $1;
} | numeric_expression
{
	$$ = $1;
} | literal_expression
{
	$$ = $1;
} | LEFT_PARENT expression RIGHT_PARENT
{
	$$ = $2;
} | array_expression
{
	$$ = $1;
} | array_access
{
	$$ = $1;
} | structured_expression
{
	$$ = $1;
};

function_call: IDENT LEFT_PARENT args RIGHT_PARENT
{
	$$ = yy.type_factory.create_function_call_expression(yy.program,$1,$3);
} | IDENT LEFT_PARENT RIGHT_PARENT
{
	$$ = yy.type_factory.create_function_call_expression(yy.program,$1,[]);
};

symbol_access: IDENT
{
	$$ = yy.type_factory.create_variable_expression($1);
} | COLON IDENT
{
	$$ = yy.type_factory.create_global_variable_expression($2);
} | symbol_access DOT IDENT
{
	$$ = yy.type_factory.create_member_access_expression($1,$3);
} | symbol_access DOT IDENT LEFT_PARENT args RIGHT_PARENT
{
	$$ = yy.type_factory.create_member_function_access_expression($1,$3,$5);
} | symbol_access DOT IDENT LEFT_PARENT RIGHT_PARENT
{
	$$ = yy.type_factory.create_member_function_access_expression($1,$3,[]);
} | symbol_access RIGHT_ARROW IDENT LEFT_PARENT args RIGHT_PARENT
{
	$$ = yy.type_factory.create_type_function_access_expression($1,$3,$5);
} | symbol_access RIGHT_ARROW IDENT LEFT_PARENT RIGHT_PARENT
{
	$$ = yy.type_factory.create_type_function_access_expression($1,$3,[]);
};

return_statement: RETURN expression
{
	$$ = yy.type_factory.create_return_statement($2);
} | RETURN
{
	$$ = yy.type_factory.create_return_statement(undefined);
};

args: args COMMA expression 
{ 
	$$ = $1.concat($3);
} | expression
{
	$$ = [ $1 ];
};

_idents: COMMA IDENT _idents
{
	$3.unshift($2);

	$$ = $3;
} | COMMA IDENT
{
	$$ = [ $2 ];
};

idents: IDENT _idents
{
	$2.unshift($1);

	$$ = $2;
};

symbol_expression: symbol_access
{
	$$ = $1;
} | function_call
{
	$$ = $1;
} | COLON function_call
{
	$$ = yy.type_factory.create_global_function_call_expression($2);
} | SHARP symbol_expression
{
	$$ = yy.type_factory.create_indexed_access_expression($2);
};

array_expression: OPEN_CLAUDATOR args CLOSE_CLAUDATOR
{
	$$ = yy.type_factory.create_array_expression($2);
} | OPEN_CLAUDATOR CLOSE_CLAUDATOR
{
	$$ = yy.type_factory.create_array_expression([]);
};

array_access: symbol_expression PERCENTAGE expression
{
	$$ = yy.type_factory.create_array_access_expression($1,$3);
};

structured_expression: OPEN_CLAUSE structured_members CLOSE_CLAUSE
{
	$$ = yy.type_factory.create_structured_data_expression($2);
} | OPEN_CLAUSE CLOSE_CLAUSE
{
	$$ = yy.type_factory.create_structured_data_expression({});
};

structured_members: structured_members COMMA IDENT COLON expression
{
	$$ = $1.concat([[$3,$5]]);
} | IDENT COLON expression
{
	$$ = [[$1,$3]];
};

literal_expression: LITERAL LITERAL_TEXT LITERAL
{
	$$ = yy.type_factory.create_literal_expression($2);
} | LITERAL LITERAL
{
	$$ = yy.type_factory.create_literal_expression("");
};

numeric_expression: NUMBER
{
	$$ = yy.type_factory.create_numeric_expression(Number($1));
};

arithmetic_expression: expression ADDITION expression
{
	$$ = yy.type_factory.create_binary_expression($1,$3,(lhs,rhsFunctor)=>{ const rhsObj = rhsFunctor(); return lhs.evaluate('sum',{other: rhsObj}); });
} | expression SUBSTRACTION expression
{
	$$ = yy.type_factory.create_binary_expression($1,$3,(lhs,rhsFunctor)=>{ const rhsObj = rhsFunctor(); return lhs.evaluate('subs',{other: rhsObj}); });
} | expression MULTIPLICATION expression
{
	$$ = yy.type_factory.create_binary_expression($1,$3,(lhs,rhsFunctor)=>{ const rhsObj = rhsFunctor(); return lhs.evaluate('prod',{other: rhsObj}); });
} | expression DIVISION expression
{
	$$ = yy.type_factory.create_binary_expression($1,$3,(lhs,rhsFunctor)=>{ const rhsObj = rhsFunctor(); return lhs.evaluate('div',{other: rhsObj}); });
} | expression POW expression
{
	$$ = yy.type_factory.create_binary_expression($1,$3,(lhs,rhsFunctor)=>{ const rhsObj = rhsFunctor(); return lhs.evaluate('pow',{other: rhsObj}); });
} | SUBSTRACTION expression
{
	$$ = yy.type_factory.create_unary_expression($2,(value)=>{ return value.evaluate('inv'); });
};

boolean_expression: expression AND expression
{
	$$ = yy.type_factory.create_binary_expression($1,$3,(lhs,rhsFunctor)=>{ if(lhs.bool()) { const rhsObj = rhsFunctor(); return rhsObj.as_bool(); } else { return lhs.as_bool(); } });
} | expression OR expression
{
	$$ = yy.type_factory.create_binary_expression($1,$3,(lhs,rhsFunctor)=>{ if(lhs.bool() == false) { const rhsObj = rhsFunctor(); return rhsObj.as_bool(); } else { return lhs.as_bool(); } });
} | expression EQUAL expression
{
	$$ = yy.type_factory.create_binary_expression($1,$3,(lhs,rhsFunctor)=>{ const rhsObj = rhsFunctor(); return lhs.evaluate('equal',{other: rhsObj}); });
} | expression NOT_EQUAL expression
{
	$$ = yy.type_factory.create_binary_expression($1,$3,(lhs,rhsFunctor)=>{ const rhsObj = rhsFunctor(); return lhs.evaluate('not_equal',{other: rhsObj}); });
} | expression OPEN_BRACKET expression
{
	$$ = yy.type_factory.create_binary_expression($1,$3,(lhs,rhsFunctor)=>{ const rhsObj = rhsFunctor(); return lhs.evaluate('less',{other: rhsObj}); });
} | expression CLOSE_BRACKET expression
{
	$$ = yy.type_factory.create_binary_expression($1,$3,(lhs,rhsFunctor)=>{ const rhsObj = rhsFunctor(); return lhs.evaluate('greater',{other: rhsObj}); });
} | expression LESS_OR_EQUAL expression
{
	$$ = yy.type_factory.create_binary_expression($1,$3,(lhs,rhsFunctor)=>{ const rhsObj = rhsFunctor(); return lhs.evaluate('less_or_equal',{other: rhsObj}); });
} | expression GREATER_OR_EQUAL expression
{
	$$ = yy.type_factory.create_binary_expression($1,$3,(lhs,rhsFunctor)=>{ const rhsObj = rhsFunctor(); return lhs.evaluate('greater_or_equal',{other: rhsObj}); });
} | NOT expression
{
	$$ = yy.type_factory.create_unary_expression($2,(value)=>{ return value.evaluate('not'); });
} | TRUE
{
	$$ = yy.type_factory.create_boolean_expression(true);
} | FALSE
{
	$$ = yy.type_factory.create_boolean_expression(false);
};