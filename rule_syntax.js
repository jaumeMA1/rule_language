const pm = require('./rule_program.js');
const ro = require('./rule_optimizer.js');
const tf = require('./rule_type_factory.js')
const fs = require("fs");

class syntax
{
	constructor(parser)
	{
		this.parser = parser;
	}
	compile(rulesFile)
	{
		this.parser.line_number = 1;
		this.parser.file_name = rulesFile;
		this.parser.yy.parser = this.parser;
		this.parser.yy.type_factory = new tf.type_factory();
		this.parser.yy.program = new pm.program();

		try
		{
			//compile rules
			this.parser.parse(fs.readFileSync(rulesFile, "utf8"));

			//optimize program
			const optmizer = new ro.syntax_optimizer();
			const optimizedProgram = optmizer.optimize(this.parser.yy.program);

			return new pm.program_interface(optimizedProgram);
		}
		catch(error)
		{
			console.error("Compilation error: " + error + ", at " + this.parser.file_name + ":" + this.parser.line_number );
			
			return undefined;
		}
	}
};

module.exports.syntax = syntax;